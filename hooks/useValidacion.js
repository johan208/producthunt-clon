import React, { useState, useEffect } from 'react';


const useValidacion = (stateInicial, validar, fn) => {
    const [valores, guardarValores ] = useState(stateInicial);
    
    const [errores, guardarErrores] = useState({});
    const [submit, guardarSubmit] = useState(false);

    useEffect( () => {
        if(submit) {
            const noErrores = Object.keys(errores).length === 0;

            if(noErrores) {
                fn(); // FN = funcion que se ejecuta en cada componente
            }
            guardarSubmit(false);
        }
    }, [errores])

    // Funcion que se ejecuta conforme el usuario escribe algo
    const handleChange = e => {
        guardarValores({
            ...valores,
            [e.target.name] : e.target.value
        })
    }

    // función que se ejecuta cuando se hace submit
    const handleSubmit = e => {
        e.preventDefault();
        const erroresValidacion = validar(valores);
        guardarErrores(erroresValidacion)
        guardarSubmit(true);
    }

    const handleBlur = () => {
        const erroresValidacion = validar(valores);
        guardarErrores(erroresValidacion);
    }

    return {
        valores,
        errores,
        submit,
        handleChange,
        handleSubmit,
        handleBlur
    };
}
 
export default useValidacion;