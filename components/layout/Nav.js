import React, { useContext } from 'react';
import Link from 'next/link';
import styled from '@emotion/styled';

import {FirebaseContext} from '../../firebase';

const NavS = styled.nav`
    padding-left: 2rem;

    a {
        font-size: 1.8rem;
        margin-left: 2rem;
        color: var(--gris2);
        font-family: 'PT Sans' ,sans-serif;

        &:last-of-type {
            margin-right:0;
        }
    }
`;

const Nav = () => {

    const { usuario } = useContext(FirebaseContext);

    return ( 
        <NavS>
            <Link href="/" > 
                {/* tiene que ir identado , no en la misma linea porque saca error  */}
                <a> Inicio </a> 
            </Link>
            <Link href="/populares" >
                <a> Populares </a>
            </Link>
            { usuario && (
                <Link href="/crear-producto" >
                    <a>Nuevo Producto</a>
                </Link>
            )}
            
        </NavS>
     );
}
 
export default Nav;