export default function validarCrearCuenta (valores) {
    
    let errores = {};

    // Validar el nombre de producto
    if(!valores.nombre) {
        errores.nombre = "El Nombre del producto es obligatorio"
    }

    if(!valores.empresa) {
        errores.empresa = "Nombre de la empresa es obligatorio"
    }
    

    if(!valores.url) {
        errores.url = "La Url del producto es obligatorio"
    } else if( !/^(ftp|http|https):\/\/[^ "]+$/.test(valores.url) ) {
        errores.url = "URL formato erroneo o invalida"
    }

    if(!valores.descripcion) {
        errores.descripcion = "Agrega una descripción a tu producto"
    }


    return errores;
}