import * as app from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/storage';

import firebaseConfig from './config';

class Firebase {
    constructor() {
        if (!app.apps.length) {
            app.initializeApp(firebaseConfig);
        }
        
        this.auth = app.auth();
        this.db = app.firestore();
        this.storage = app.storage();
    }

    //Registrar usuario
    async registrar (nombre,email,password) {
        console.log(firebase);
        console.log(this.auth);
        const nuevoUsuario = await this.auth.createUserWithEmailAndPassword(email, password);

        return await nuevoUsuario.user.updateProfile({
            displayName: nombre
        });
    }

    // Iniciar sesion
    async login(email,pass) {

        return this.auth.signInWithEmailAndPassword(email, pass)
    }

    // Cerrar Sesion
    async cerrarSesion() {
        await this.auth.signOut();
    }

}

const firebase = new Firebase();

export default firebase;