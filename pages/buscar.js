import React, { useEffect, useState } from 'react';
import Layout from '../components/layout';
import { useRouter} from 'next/router';

import useProductos from '../hooks/useProductos';
import DetalleProducto from '../components/layout/DetalleProducto';


export default function Buscar() {

  const router = useRouter();
  const { query: { q }} = router;

  // todos los productos
  const { productos } = useProductos('creado');
  const [ resultados , guardarResultados] = useState([]);
  useEffect( () => {

    const busqueda = q.toLowerCase();
    const filtro = productos.filter(producto => {
      return (
        producto.nombre.toLowerCase().includes(busqueda) || producto.descripcion.toLowerCase().includes(busqueda)
      )
    });
    console.log(filtro);
    guardarResultados(filtro);

  }, [q, productos])

  return (
    <div>
        <Layout>
            <div className="listado-productos">
                <div className="contenedor">
                    <ul className="bg-white">
                        { resultados.map(producto => (
                            <DetalleProducto 
                                key={producto.id}
                                producto={producto}
                            />
                        ))
                        }
                    </ul>
                </div>
            </div>
        </Layout>
    </div>
  )
}