import React, { useState } from 'react';
import { css } from '@emotion/core';
import Router from 'next/router';
import Layout from '../components/layout';
import { Formulario, Campo, InputSubmit, Error } from '../components/ui/Formulario';

import firebase from '../firebase';

// Validaciones
import useValidacion from './../hooks/useValidacion';
import validarCrearCuenta from './../validacion/validarCrearCuenta';

const STATE_INICIAL = {
    nombre: '',
    email: '',
    password: ''
};

const CrearCuenta = (props) => {

    const [ existeusuario, guardarExisteUsuario] = useState(false);

    // Funcion crear la cuenta
    const crearCuenta = async () => {
        try {
            await firebase.registrar(nombre, email, password);
            Router.push('/');
            console.log('usuario creado')
        } catch(error) {
            console.error('Hubo un error al registrar el usuario',error.message)
            guardarExisteUsuario(error.message);
        }
        
    };

    const { valores,
        errores,
        handleChange,
        handleSubmit,
        handleBlur } = useValidacion( STATE_INICIAL, validarCrearCuenta, crearCuenta);

    const { nombre, email, password} = valores;

    

    return ( 
        <div>
            <Layout>
                <>
                    <h1
                        css={css`
                        text-align: center;
                        margin-top: 5rem;
                        `}
                    >Crear Cuenta</h1>
                    <Formulario
                        onSubmit={handleSubmit}
                        noValidate
                    >
                        <Campo>
                            <label htmlFor="nombre">Nombre</label>
                            <input 
                                type="text"
                                id="nombre"
                                placeholder="Tu Nombre"
                                name="nombre"
                                value={nombre}
                                onChange={handleChange}
                                onBlur={handleBlur}
                            />
                        </Campo>

                        {errores.nombre && <Error>{errores.nombre}</Error> }
            
                        <Campo>
                            <label htmlFor="email">Email</label>
                            <input 
                                type="email"
                                id="email"
                                placeholder="Tu Email"
                                name="email"
                                value={email}
                                onChange={handleChange}
                                onBlur={handleBlur}
                            />
                        </Campo>
                        {errores.email && <Error>{errores.email}</Error> }
            
                        <Campo>
                            <label htmlFor="password">Password</label>
                            <input 
                                type="password"
                                id="password"
                                placeholder="Tu Password"
                                name="password"
                                value={password}
                                onChange={handleChange}
                                onBlur={handleBlur}
                            />
                        </Campo>
                        {errores.password && <Error>{errores.password}</Error> }

                        {existeusuario && <Error>{existeusuario} </Error>}
            
                        <InputSubmit 
                            type="submit"
                            value="Crear Cuenta"
                        />
                    </Formulario>
                </>
            </Layout>
        </div>
     );
}
 
export default CrearCuenta;