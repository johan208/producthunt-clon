import React, { useEffect, useState, useContext } from 'react';
import { useRouter } from 'next/router';
import formatDistanceToNow from 'date-fns/formatDistanceToNow';
import { es } from 'date-fns/locale';
import { css } from '@emotion/core';
import styled from '@emotion/styled';

import Layout from '../../components/layout'
import Error404 from '../../components/layout/404';
import { Campo, InputSubmit } from '../../components/ui/Formulario';
import Button from '../../components/ui/Button'

import { FirebaseContext } from '../../firebase';


const ContenedorProducto = styled.div`
    @media ( min-width:768px ) {
        display: grid;
        grid-template-columns: 2fr 1fr;
        column-gap: 2rem;
    }
`;

const CreadorProducto = styled.p`
    color: #fff;
    text-transform: uppercase;
    font-weight: bold;
    letter-spacing: .2rem;
    display:inline-block;
    font-size: 1rem;
    

    span {
        padding: .4rem .8rem;
        background-color: #DA552F;
        text-align: center;
        border-radius:15px;
        
    }
`;

const Producto = () => {

    // State del componente
    const [ producto, guardarProducto ] = useState({});
    const [ error, guardarError] = useState(false);
    const [ comentario, guardarComentario] = useState({});
    const [ consultardb, guardarConsultarDB] = useState(true);

    // Routing para obtener el id actual
    const router = useRouter();
    const { query: {id}} = router;

    // Context de firebase
    const { firebase, usuario } = useContext(FirebaseContext);

    useEffect( () => {
        if(id && consultardb) {

            const obtenerProducto = async () => {

                const productoQuery = await firebase.db.collection('productos').doc(id);
                const producto = await productoQuery.get();
                if ( producto.exists ) {
                    guardarProducto(producto.data());
                    guardarConsultarDB(false);
                } else {
                    guardarError(true);
                }
            }
            obtenerProducto();
        }
    }, [id])

    const { comentarios, creado, creador, descripcion, empresa, nombre, url, urlimagen, votos, haVotado} = producto;

    //validacion para que no hayan errores con als funciones  de map y tiempo cuando carga la pagina
    //porque cuando carga la pagina inicia en null y las funciones se ejecutan sobre algo que no existe aun
    //si no se usa esta validacion puede ser con un ternario en la renderizacion del elemento.

    if (Object.keys(producto).length === 0 && !error ) return 'Cargando ...'

    const votarProducto = () => {
        console.log("votando...");
        if( !usuario ) {
            return router.push('/login');
        }

        // obtener y sumar votos
        const nuevoTotal = votos + 1;

        // Verificar si el usuario actual ha votado
        if( haVotado.includes(usuario.uid) ) return alert('Solo se puede votar 1 sola vez');

        // Guardar el ID del usuario que voto
        const nuevoHaVotado = [...haVotado, usuario.uid];

        //  Actualziar BD
        firebase.db.collection('productos').doc(id).update({
            votos: nuevoTotal,
            haVotado: nuevoHaVotado
        });
        //  Actualziar state
        guardarProducto({
            ...producto,
            votos: nuevoTotal,
        })

        guardarConsultarDB(true); // cuando hay voto se consulta db otra vez
    }

    const comentarioChange = e => {
        guardarComentario({
            ...comentario,
            [e.target.name] : e.target.value
        })
    }

    // Identifica si el comentario es del creador del producto
    const esCreador = id => {
        if(creador.id == id) {
            return true;
        }
    }

    const agregarComentario = e => {
        e.preventDefault();

        if( !usuario ) {
            return router.push('/login')
        }

        // informacion extra al comentario
        comentario.usuarioId = usuario.uid;
        comentario.usuarioNombre = usuario.displayName;

        // Tomar copia de comentarios y agregarlos al arreglo
        const nuevosComentarios = [...comentarios, comentario];

        //  Actualizar BD
        firebase.db.collection('productos').doc(id).update({
            comentarios: nuevosComentarios
        })

        //  Actualizar state
        guardarProducto({
            ...producto,
            comentarios: nuevosComentarios
        })

        guardarConsultarDB(true); // cuando se agg comentario se activa la consulta a la bd

    }

    // funcion que revisa el creador del producto sea el mismo que esta autenticado
    const paraEliminar = () => {
        if ( !usuario ) return false

        if( creador.id === usuario.uid) {
            return true;
        }
    }

    const eliminarProducto = async () => {

        if ( !usuario ) {
            return router.push('/login');
        } 

        if ( creador.id === usuario.id) {
            return router.push('/');
        }

        try {
            await firebase.db.collection('productos').doc(id).delete();
            router.push('/');

        } catch ( error ) {
            console.log(error);
        }
    }

    return ( 
        <Layout>
            <>
                { error ? <Error404 /> : (
                    <div className="contenedor">
                    <h1 css={css`
                        text-align: center;
                        margin-top: 5rem;
                    `}>
                        { nombre }
                    </h1>

                    <ContenedorProducto>
                        <div>
                            <p> Publicado por :&nbsp; {creador.nombre} </p>
                            { creado ? <p> Hace:&nbsp; { formatDistanceToNow(new Date(creado), { locale: es } )} </p> : null}
                            <img  src={urlimagen} />
                            <p> {descripcion} </p>


                            {usuario && (
                                <>
                                    <h2> Agrega tu comentario </h2>
                                    <form 
                                        onSubmit={agregarComentario}
                                    >
                                        <Campo>
                                            <input 
                                                type="text"
                                                name="mensaje"
                                                onChange={comentarioChange}
                                            />
                                        </Campo>
                                        <InputSubmit
                                            type="submit"
                                            value="Agregar comentario"
                                            
                                        />
                                    </form>
                                </>
                            )}
                            

                            <h2 
                                css={css`
                                    margin: 2rem 0;
                                `}
                            > Comentarios </h2>
                            {comentarios.length === 0 ? " No hay comentarios " : (
                                <ul>
                                    { comentarios.map(( comentario, i ) => (
                                        <li
                                            key={`${comentario.usuarioId}-${i}`}
                                            css={css`
                                                border: 1px solid #e1e1e1;
                                                border-radius:10px;
                                                padding: 2rem;
                                            `}
                                        >
                                            <p css={css`
                                                display:inline-block;
                                                margin-right: 1.5rem;
                                            `}> 
                                                <span
                                                    css={css`
                                                        font-weight:bold;
                                                    `}    
                                                >
                                                    {comentario.usuarioNombre}
                                                </span> 
                                            </p>
                                            { esCreador(comentario.usuarioId) && (
                                                <CreadorProducto><span> Autor </span></CreadorProducto>
                                            )}
                                            <p> {comentario.mensaje} </p>
                                            
                                            
                                        </li>
                                    )) }
                                </ul>
                            )}

                            
                        </div>

                        <aside>
                            
                                <Button
                                    target="_blank"
                                    bgColor="true"
                                    href={url}
                                >
                                    Visitar URL
                                </Button>
                            <div css={css`
                                margin-top: 5rem;
                            `}>
                                <p css={css`
                                    text-align: center;
                                `}>
                                    {votos} Votos
                                </p>
                                
                                {usuario && (
                                    <Button
                                        onClick={votarProducto}
                                    >
                                    Votar
                                    </Button>
                                )}
                                
                            </div>
                        </aside>
                    </ContenedorProducto>
                    { paraEliminar() && 
                        <Button
                            onClick={eliminarProducto}
                        >   Eliminar Producto 
                        </Button>
                    }
                </div>
                )}

                
            </>
        </Layout>
        
     );
}
 
export default Producto;